package malprox

import (
	"bufio"
	"log"
	"os"
	"strings"
)

// Manages the loaded list of bad (blacklisted) URLs.
type FlatfileDB struct {
	badUrls map[string]int
}

// Initialize and return an empty database.
func NewFlatfileDB() *FlatfileDB {
	db := new(FlatfileDB)
	db.badUrls = make(map[string]int)
	return db
}

// Initialize and return a malprox database using the given file.  Returns
// an error if reading the file failed.
func NewFlatfileDBFromFile(file string) (*FlatfileDB, error) {
	db := NewFlatfileDB()
	err := db.LoadUrlFile(file)
	if err != nil {
		return nil, err
	}
	return db, nil
}

// Load the given URL blacklist file.  This may be called more than once for
// multiple files; it will simply merge later files' entries into the database.
func (db *FlatfileDB) LoadUrlFile(filename string) error {
	fh, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer fh.Close()

	linenum := 0
	badnum := 0
	scanner := bufio.NewScanner(fh)
	for scanner.Scan() {
		linenum++
		line := strings.TrimLeft(scanner.Text(), " \t")
		if len(line) < 1 || line[0] == '#' {
			// comment or empty line
			continue
		}
		// TODO: Some kind of validation (would increment badnum)
		db.badUrls[string(line)] = 0 // int value currently unused
	}
	log.Printf("Loaded \"%s\": %d entries ok, %d malformed, %d total lines", filename, len(db.badUrls), badnum, linenum)
	return nil
}

// Test if the given URL is in our blacklist.
func (db *FlatfileDB) CheckIfFlagged(subject *string) (bool, error) {
	_, exists := db.badUrls[*subject]
	return exists, nil
}
