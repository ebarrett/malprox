package malprox

// General interface for all URL lookup "database" implementations in malprox
type Database interface {
	CheckIfFlagged(subject *string) (bool, error)
}
