package malprox

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"log"
)

// Manages the loaded list of bad (blacklisted) URLs.
type MySQLDB struct {
	server   string
	port     int
	user     string
	password string
	db       string
	table    string

	// Generated once for efficiency
	dsn string
}

// Initialize and return an empty database.
func NewMySQLDB(server string, port int, user string, password string, db string, table string) *MySQLDB {
	mdb := new(MySQLDB)
	mdb.server = server
	mdb.port = port
	mdb.user = user
	mdb.password = password
	mdb.db = db
	mdb.table = table

	mdb.dsn = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", mdb.user, mdb.password, mdb.server, mdb.port, mdb.db)
	return mdb
}

// Test if the given URL is in our blacklist.
func (mdb *MySQLDB) CheckIfFlagged(subject *string) (bool, error) {
	// Go's sql driver uses a connection pool, so this is theoretically
	// efficient
	conn, err := sql.Open("mysql", mdb.dsn)
	if err != nil {
		log.Printf("ERROR: Cannot connect to MySQL server %s@%s:%s: %s", mdb.user, mdb.server, mdb.port, err.Error())
		return true, err

	}
	defer conn.Close()

	// This could be a LIKE or some other kind of match depending on how you
	// want to match URLs and how much CPU time you want to spend on the DB
	row := conn.QueryRow("SELECT `url` FROM `badurls` WHERE `url` = ? LIMIT 1", *subject)
	var matched_url string
	err = row.Scan(&matched_url)
	switch {
	case err == sql.ErrNoRows:
		// No match
		log.Printf("No match for `%s' in database", *subject)
		return false, nil
	case err != nil:
		log.Printf("ERROR: MySQL query failed: %s", err.Error())
		return true, err
	}
	// Matched
	log.Printf("Matched query URL `%s' with `%s'", *subject, matched_url)
	return true, nil
}
