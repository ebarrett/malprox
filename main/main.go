package main

import (
	"bitbucket.org/ebarrett/malprox"
	"log"
	"net/http"
)

func main() {
	// Get command-line flags
	c := new(malprox.Config)
	c.Init()

	var mdb malprox.Database
	if len(c.UrlFile) > 0 {
		// Load list of bad URLs
		var err error
		mdb, err = malprox.NewFlatfileDBFromFile(c.UrlFile)
		if err != nil {
			log.Fatalf("Error reading URL list \"%s\": %s", c.UrlFile, err.Error())
		}
	} else {
		mdb = malprox.NewMySQLDB(c.MySQLServer, c.MySQLPort, c.MySQLUser, c.MySQLPassword, c.MySQLDB, c.MySQLTable)
	}

	// Set up request handler
	mph := malprox.NewHandler(mdb)
	http.Handle(c.BasePath, http.StripPrefix(c.BasePath, mph))

	// This function never returns unless there's a problem
	log.Printf("Server listening on port %s, base URL %s", c.Port, c.BasePath)
	err := http.ListenAndServe(c.Port, nil)
	if err != nil {
		log.Fatalf("Unexpected server exit: %s", err.Error())
	}
	log.Fatal("Unexpected server exit (no error?)")
}
