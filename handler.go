package malprox

import (
	"encoding/json"
	"log"
	"net/http"
)

// Implements the http.Handler interface that receives URL validation requests
// from an external web proxy.
type Handler struct {
	// Malprox database which we'll use to look up subject URLs
	db Database
}

// MalproxResponse is used to marshall the JSON response.
type MalproxResponse struct {
	// True if URL was in the blacklist
	Flagged bool `json:"flagged"`

	// The URL (path) which was examined
	Subject string `json:"subject"`

	// If `error` is set, the query failed:
	//   - `flagged` becomes undefined and should now be ignored
	//   - `errormsg` will be included with the text of the error
	Error    bool   `json:"error"`
	ErrorMsg string `json:"errormsg,omitempty"`
}

// Fallback error in case JSON conversion fails (unlikely?)
var jsonErrorResp []byte = []byte("{flagged:true,subject:\"\",error:true,errormsg:\"JSON conversion failed\"}")

// Return a newly initialized Malprox handler.  Must be supplied with a
// malprox.Database.
func NewHandler(db Database) *Handler {
	h := new(Handler)
	h.db = db
	return h
}

// Handles HTTP requests.  Expects the http.Request URL to have the base
// path stripped off before it's called.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	subjectUrl := r.URL.Path
	log.Printf("Got request to validate `%s'", subjectUrl)
	httpStatus := http.StatusOK

	// Build our response struct
	isFlagged, err := h.db.CheckIfFlagged(&subjectUrl)
	mResp := &MalproxResponse{isFlagged, subjectUrl, false, ""}
	if err != nil {
		mResp.Flagged = true
		mResp.Error = true
		mResp.ErrorMsg = err.Error()
		httpStatus = http.StatusServiceUnavailable
	}

	// Convert response struct to JSON text
	respBytes, err := json.Marshal(mResp)
	if err != nil {
		// Probably an unlikely failure, but we've cleverly constructed a
		// static fallback response just in case!
		w.WriteHeader(http.StatusInternalServerError)
		w.Write(jsonErrorResp)
		return
	}

	// Send the response
	w.WriteHeader(httpStatus)
	w.Write(respBytes)
}
