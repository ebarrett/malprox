-- Creates and populates the malprox database and table on a MySQL server.

-- DROP DATABASE `malprox`;
CREATE DATABASE `malprox`;
USE `malprox`;
CREATE TABLE `badurls` (
      `id` bigint(20) NOT NULL AUTO_INCREMENT,
      `url` varchar(512) NOT NULL,
      `time_added` datetime NOT NULL,
      PRIMARY KEY (`id`),
      UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB;
INSERT INTO `badurls` VALUES
    (NULL, 'example.com/this_is_bad/', CURRENT_TIMESTAMP()),
    (NULL, 'example.com:8080/this_is_worse/', CURRENT_TIMESTAMP()),
    (NULL, 'example.net/this is like totally awful/', CURRENT_TIMESTAMP());
