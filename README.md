# malprox: A URL-classifying server #

Copyright (C) 2015 by Eric Barrett (arctil@gmail.com).  All rights reserved.
Redistribution permitted only with authorization.


## About ##

malprox is a URL-checking HTTP server.  When an HTTP request comes, it will
check the site named in the request's path against a blacklist and respond with
a JSON object in the body describing whether it has been flagged (blacklisted)
or not.  It was written as an exercise for an OpenDNS employment test.

Because the response format was left open, I chose to use JSON.  It is almost
universally supported, relatively inexpensive to generate, and flexible enough
to extend in the future.  Should this server truly need to scale (e.g.
millions of requests per second) it would probably be more economical to
respond via a simple byte encoding, or perhaps entirely through HTTP status
codes with an empty body.

malprox is written in Go.  Though it's not my strongest language (that would be
Python), I chose it because of its excellent net.http package, its strong
pedigree in web services, its relative ease of reading, and its great
documentation support.


## Building ##

To download and build malprox:

    mkdir -p $GOPATH/bitbucket.org/ebarrett/malprox
    cd $GOPATH/bitbucket.org/ebarrett/malprox
    git clone <repo URL>
    go build -o malprox main/main.go


## Changelog ##

### 2015-08-07 ###
The first release is quite minimal.  It works out of a flat text file that must
be loaded entirely into the server's memory.  This obviously will not scale to
more than a few million blacklisted URLs.  Matches are explicit; there is no
pattern-matching.

There is no interpretation of the requested URL; the external proxy server and
whatever populates the list of bad URLs must agree on conventions such as
whether port 80 is explicitly included in a query ("example.com:80").

Spaces and other encoded characters in the request are not handled well;
they're currently subject to the net.http package's pecularities.

### 2015-10-22 ###
I started thinking about scaling.  The obvious answer is to put the lookup
service behind haproxy (or similar) and use a back-end database.  I chose
MySQL since it's straightforward and ubiquitous.  In writing this I also
prepared the otherwise-quite-trivial code for different lookup engines, so the
framework is in place for other possible stores; perhaps a NoSQL engine (ugh).

Having MySQL support provides straightforward ways to scale:
- No more flatfile that has to be read entirely and stored in memory
- Lookup server overloaded?  Build more out and put them behind haproxy
- MySQL server overloaded?  Build replicas and query those instead
- Updating the URL file becomes trivial and requires no downtime on the part of malprox

Now the limitation seems to be the matching method, which is very simple string
equality.  You could use LIKE instead of a strict comparison (and probably
would, in the real world).

NOTE: The mysql driver can be installed with:

    go get github.com/go-sql-driver/mysql

I've also provided a MySQL script to create and popualate the database in
`support/mysql_malprox_db_setup.sql`.


### 2015-11-02 ###
The final touch for this test is adding better error handling.  There's a new
JSON field which is the boolean field `error`.  If set, there will also be an
`errormsg` field with the text of the problem.  Also, the requested URL will
always be marked flagged in the event of an error so that we "fail safe."
Finally, we'll set the HTTP status code to 5xx in the event of some kind of
failure.

This is the best kind of update, in which I had a bunch of convoluted code and
added an extra flag etc. before the current, much more elegant solution
occurred to me and I got to make the diff 1/4 of the original size.

REMAINING THEORETICAL TODOS:

* The matching algorithm remains extremely primitive as discussed above.  This
  is a tricky subject what with code pages/unicode/punycode and decades of
  conflicting standards, so I didn't want to tackle it unless I was going to do
  it right.

* Tests of some kind would be nice.  The code is small and use of the http
  package would require a some shimming (probably worthwhile anyway if this was
  Real Life).  But some automated blackbox testing scripts would be a quick,
  useful way to validate that changes didn't break anything.
