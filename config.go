package malprox

import (
	"flag"
	"fmt"
	"log"
)

// The Config struct encapsulates runtime configuration values.  Currently
// these are limited to command-line flags.
type Config struct {
	// Base path of requests; this is what we expect the proxy server to always
	// prefix its URL subject requests with.
	BasePath string

	// Port on which we'll listen.
	Port string

	// Filename we'll read for flagged URLs (if any)
	UrlFile string

	// MySQL options
	MySQLServer   string
	MySQLPort     int
	MySQLUser     string
	MySQLPassword string
	MySQLDB       string
	MySQLTable    string
}

// Check if a TCP/IP port is in valid range 1-65535 (inclusive)
func PortInRange(port int) bool {
	return port >= 1 && port <= 65535
}

// Process command-line flags.  This should only be called once per execution.
func (c *Config) Init() {
	if flag.Parsed() {
		panic("Command-line args processed twice")
	}

	port := flag.Int("port", 8080, "TCP port where malprox server listens")
	basePath := flag.String("basepath", "/urlinfo/1/", "Base URL path for proxy requests")
	urlFile := flag.String("urlfile", "", "Flat text file containing list of bad URLs")
	mysqlServer := flag.String("mysql-server", "", "MySQL server hostname")
	mysqlPort := flag.Int("mysql-port", 3306, "MySQL server port")
	mysqlUser := flag.String("mysql-user", "mysql", "MySQL user name")
	mysqlPassword := flag.String("mysql-pass", "", "MySQL user password")
	mysqlDb := flag.String("mysql-db", "malprox", "MySQL bad-url database name")
	mysqlTable := flag.String("mysql-table", "badurls", "MySQL bad-url lookup table name")

	flag.Parse()

	// Check proxy port range
	if !PortInRange(*port) {
		log.Fatal("--port is not a valid TCP/IP port number")
	}
	if len(*urlFile) == 0 && len(*mysqlServer) == 0 {
		log.Fatal("Please specify either --urlfile or --mysql-server")
	}
	// Check flatfile || MySQL
	if len(*urlFile) > 0 && len(*mysqlServer) > 0 {
		log.Fatal("Cannot specify both a flatfile (--urlfile) and a MySQL server (--mysql-server)")
	}

	// Add trailing slash to --baseurl if needed
	if len(*basePath) < 1 {
		log.Fatal("--basepath must not be empty")
	}
	if (*basePath)[len(*basePath)-1] != '/' {
		*basePath += "/"
	}

	// Validate MySQL settings
	if len(*mysqlServer) > 0 {
		if !PortInRange(*mysqlPort) {
			log.Fatal("--mysql-port is not a valid TCP/IP port number")
		}
		if len(*mysqlDb) < 1 {
			log.Fatal("Must specify a valid MySQL database (--mysql-db)")
		}
		if len(*mysqlTable) < 1 {
			log.Fatal("Must specify a valid MySQL table (--mysql-table)")
		}
		if len(*mysqlUser) < 1 {
			log.Fatal("Must specify a valid MySQL user (--mysql-user)")
		}
	}

	c.Port = fmt.Sprintf(":%d", *port)
	c.BasePath = *basePath
	c.UrlFile = *urlFile
	c.MySQLServer = *mysqlServer
	c.MySQLPort = *mysqlPort
	c.MySQLUser = *mysqlUser
	c.MySQLPassword = *mysqlPassword
	c.MySQLDB = *mysqlDb
	c.MySQLTable = *mysqlTable

	// Log flags (except password) after processing so the logfile has complete
	// context
	log.Printf("Config: port=%s, basepath=%s, urlfile=%s, mysql-server=%s, mysql-port=%d, mysql-db=%s, mysql-table=%s", c.Port, c.BasePath, c.UrlFile, c.MySQLServer, c.MySQLPort, c.MySQLDB, c.MySQLTable)
}
